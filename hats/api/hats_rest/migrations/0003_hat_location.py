# Generated by Django 4.0.3 on 2023-07-20 18:34

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('hats_rest', '0002_locationvo_alter_hat_url'),
    ]

    operations = [
        migrations.AddField(
            model_name='hat',
            name='location',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='hats', to='hats_rest.locationvo'),
        ),
    ]
