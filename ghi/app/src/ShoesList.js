import React, {useEffect, useState} from "react";
import "./index.css";

function ShoesList() {
    const deletesShoe = async (shoe) => {
        const shoesUrl = "http://localhost:8080/shoes/${shoe.id}/"
        const fetchConfig = {method: "delete"}
        const response = await fetch(shoesUrl, fetchConfig)
        if (response.ok) {
            const data = await response.json();
            setShoes(data.shoes)

        }
    }
    if (props.shoes === undefined) {
        return null;
    }

    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Shoe picture</th>
                    <th>Manufacturer</th>
                    <th>Name</th>
                    <th>Color</th>
                    <th>Closet Name</th>
                </tr>
            </thead>
            <tbody>
                {props.shoes.map(shoe => {
                    return (
                        <tr key ={shoe.id}>
                            <td>{shoe.manufacturer}</td>
                            <td>{shoe.model_name}</td>
                            <td>{shoe.color}</td>
                            <td>{shoe.closet_name}</td>
                            <td><img src={shoe.picture_url} alt="" width="40%" height="40%"/></td>
                            <td>
                            <button onClick={() => {DeleteShoe(shoe.id)}} type = "button" className="btn btn-danger">Delete</button>
                            </td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
    );
    }

export default ShoesList;
