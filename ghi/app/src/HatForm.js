import React, {useEffect, useState } from 'react';

function HatForm() {

  const [location, setLocation] = useState('');
  const [styleName, setStyleName] = useState('');
  const [fabric, setFabric] = useState('');
  const [color, setColor] = useState('');
  const [url, setUrl] = useState('');
  const [locations, setLocations] = useState([]);
  const [hasSignedUp, setHasSignedUp] = useState(false);

  const fetchData = async () => {
    const url = 'http://localhost:8100/api/locations/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setLocations(data.locations);
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {};
    data.location = location;
    data.style_name = styleName;
    data.color = color;
    data.url = url;
    data.fabric = fabric;

    const hatUrl = 'http://localhost:8090/api/hats/';
    const fetchOptions = {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const hatResponse = await fetch(hatUrl, fetchOptions);
    if (hatResponse.ok) {
      setLocation('');
      setStyleName('');
      setColor('');
      setUrl('');
      setFabric('');
      setHasSignedUp(true);
    }
  }

  const handleChangeLocation = (event) => {
    const value = event.target.value;
    setLocation(value);
  }

  const handleChangeStyleName = (event) => {
    const value = event.target.value;
    setStyleName(value);
  }

  const handleChangeColor = (event) => {
    const value = event.target.value;
    setColor(value);
  }

  const handleChangeUrl = (event) => {
    const value = event.target.value;
    setUrl(value);
  }

  const handleChangeFabric = (event) => {
    const value = event.target.value;
    setFabric(value);
  }

  let spinnerClasses = 'd-flex justify-content-center mb-3';
  let dropdownClasses = 'form-select d-none';
  if (locations.length > 0) {
    spinnerClasses = 'd-flex justify-content-center mb-3 d-none';
    dropdownClasses = 'form-select';
  }

  let messageClasses = 'alert alert-success d-none mb-0';
  let formClasses = '';
  if (hasSignedUp) {
    messageClasses = 'alert alert-success mb-0';
    formClasses = 'd-none';
  }

  return (
    <div className="my-5 container">
      <div className="row">
        <div className="col">
          <div className="card shadow">
            <div className="card-body">
              <form className={formClasses} onSubmit={handleSubmit} id="create-hat-form">
                <h1 className="card-title">It is about that time.. Hat Time!</h1>
                <p className="mb-3">
                  Here, you choose the location of your hat
                </p>
                <div className={spinnerClasses} id="loading-location-spinner">
                  <div className="spinner-grow text-secondary" role="status">
                    <span className="visually-hidden">Loading...</span>
                  </div>
                </div>
                <div className="mb-3">
                  <select onChange={handleChangeLocation} name="location" id="location" className={dropdownClasses} required>
                    <option value="">Pick a location for your hat</option>
                    {locations.map(location => {
                      return (
                        <option key={location.href} value={location.href}>{location.closet_name}</option>
                      )
                    })}
                  </select>
                </div>
                <p className="mb-3">
                  Let us hear more about that hat of yours
                </p>
                <div className="row">
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleChangeStyleName} required placeholder="Hat name" type="text" id="style_name" name="style_name" className="form-control" />
                      <label htmlFor="name">Hat name</label>
                    </div>
                  </div>
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleChangeColor} required placeholder="Hat color" type="text" id="color" name="color" className="form-control" />
                      <label htmlFor="color">Hat color</label>
                    </div>
                  </div>
                </div>

                <div className="row">
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleChangeUrl} required placeholder="Hat url" type="text" id="url" name="url" className="form-control" />
                      <label htmlFor="name">Hat url</label>
                    </div>
                  </div>
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleChangeFabric} required placeholder="Hat fabric" type="text" id="fabric" name="fabric" className="form-control" />
                      <label htmlFor="color">Hat fabric</label>
                    </div>
                  </div>
                </div>

                <button className="btn btn-lg btn-primary">WooHoo, my new favorite hat incoming!</button>
              </form>
              <div className={messageClasses} id="success-message">
                I am proud of you! Kudos on your new hat friend
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default HatForm;
