function hatsApp(props) {
    return (
        <div className="container my-5">
            <table className="table">
                <thead>
                    <tr className="bg-primary">
                        <th>Color</th>
                        <th>Fabric</th>
                        <th>Style Name</th>
                        <th>Locations</th>
                        <th>URL</th>
                    </tr>
                </thead>
                <tbody>
                    {props.hats && props.hats.map(hat => {
                        return (
                            <tr className="table-info" key={hat.href}>
                                <td>{ hat.color}</td>
                                <td>{ hat.fabric}</td>
                                <td>{ hat.style_name}</td>
                                <td>{ hat.location }</td>
                                <td>{ hat.url}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    )
}

export default hatsApp;
