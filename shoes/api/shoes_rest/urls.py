from django.urls import path
from .views import shoes_details, shoes_list

urlpatterns = [
    path("shoes//<int:id>/", shoes_details, name="shoes_details"),
    path("shoes/", shoes_list, name="shoes_list"),
]