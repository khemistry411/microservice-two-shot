from shoes_rest.models import Shoe, BinVO
import json
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
from django.http import JsonResponse


# Create your views here.
class BinVoDetailEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "import_href",
        "closet_name",
        "bin_number",
        "bin_size",
    ]


class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "model_name",
        "color",
        "picture",
        "id",
    ]

    def get_extra_data(self, o):
        return {"bin": o.bin.closet_name}


class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "model_name",
        "color",
        "picture",
        "bin",
    ]

    encoders = {"bin": BinVoDetailEncoder()}


@require_http_methods(["GET", "POST"])
def shoes_list(request):
    if request.method == "GET":
        shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeListEncoder,
            sale=False,
        )
    else:
        content = json.loads(request.body)
        try:
            bin_href = content["bin"]
            bin = BinVO.objects.get(import_href=bin_href)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin id"},
                status=400,
            )
        shoes = Shoe.objects.create(**content)
        return JsonResponse(
            shoes,
            encoder=ShoeDetailEncoder,
            safe=False
        )


@require_http_methods(["GET", "DELETE"])
def shoes_details(request, id):
    if request.method == "GET":
        shoes = Shoe.objects.get(id=id)
        return JsonResponse(
            shoes,
            encoder=ShoeDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Shoe.objects.filter(id=id).delete()
        return JsonResponse({"Deleted": count > 0})
    else:
        content = json.loads(request.body)
        Shoe.objects.filter(id=id).update(**content)
        return JsonResponse(
            Shoe,
            encoder=ShoeDetailEncoder,
            safe=False
        )
