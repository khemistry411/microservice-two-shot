from django.contrib import admin
from shoes_rest.models import Shoe, BinVO


# Register your models here.
@admin.register(Shoe)
class ShoeAdmin(admin.ModelAdmin):
    pass


@admin.register(BinVO)
class BinVOadmin(admin.ModelAdmin):
    pass