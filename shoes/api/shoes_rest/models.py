from django.db import models


# Create your models here.
class BinVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    closet_name = models.CharField(max_length=200)
    bin_number = models.PositiveBigIntegerField()
    bin_size = models.PositiveBigIntegerField()

    def __str__(self):
        return f"{self.closet_name} - {self.bin_number}/{self.bin_size}"


class Shoe(models.Model):
    manufacturerer = models.CharField(max_length=200)
    model_name = models.CharField(max_length=200)
    color = models.CharField(max_length=200)
    picture = models.URLField(null=True)
    bin = models.ForeignKey(BinVO, related_name="shoe",
                            on_delete=models.CASCADE,
                            )

    def __str__(self):
        return f"{self.model_name}"
